#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os
import json
import uuid
import time
import datetime
import random

# other imports
from api.helpers.helpers import getDynamoTable


#####
# GET SAMPLE QUESTIONS HELPER FUNCTIONS
#####

# get sample questions from dynamodb
def postQuestion(request):
    
    # get question to add
    question = json.loads(request.body)
    
    # connect to dynamodb
    table = getDynamoTable('QUESTIONS_TABLE')
    
    # generate unique values for question
    questionId = str(uuid.uuid4())
    timestamp = str(time.time())
    randomInt = random.randint(0, 1000000)
    
    # check if question is multiple choice or not
    multipleChoice = False
    count = 0
    for key in question['correctAnswers']:
        if question['correctAnswers'][key]:
            count += 1
    if count > 1:
        multipleChoice = True
        
    # create question json
    item = {
        'id': questionId,
        'timestamp': timestamp,
        'randomInt': randomInt,
        'type': 'samplequestion',
        'user': question['username'],
        'question': question['text'],
        'answers': question['answers'],
        'correctAnswers': question['correctAnswers'],
        'multipleChoice': multipleChoice,
        'categories': question['categories'],
        'explanation': question['explanation']
    }
    
    # try to post question
    # if there is an error, send it back
    # update the last ten questions table with this most recent question
    try:
        response = table.put_item(
            Item=item,
            ConditionExpression='attribute_not_exists(id)'
        )
        updateLastTenQuestionsTable(item)
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # reply indicating question was posted successefully
    return JsonResponse({
            'message': 'all okay',
            'status': 200
        })
        
def updateLastTenQuestionsTable(item):
    
    # connect to dynamo table
    table = getDynamoTable('LAST_TEN_QUESTIONS_TABLE')
    
    # first get all questions
    response = table.query(
        KeyConditionExpression='id = :partitionkeyval',
        ExpressionAttributeValues = {
            ':partitionkeyval': 'lastTenQuestion',
        },
        ScanIndexForward=False,
    )
    
    # then remove questions until there are only 9 questions
    items = len(response['Items'])
    while items >= 10:
        
        table.delete_item(
            Key={
                'id': response['Items'][(items - 1)]['id'],
                'timestamp': response['Items'][(items - 1)]['timestamp']
            }
        )
        
        response = table.query(
            KeyConditionExpression='id = :partitionkeyval',
            ExpressionAttributeValues = {
                ':partitionkeyval': 'lastTenQuestion',
            },
            ScanIndexForward=False,
        )
        
        items = len(response['Items'])
        
    # trim text if needed
    if len(item['question']) > 100:
        item['question'] = item['question'][:100]
    
    # finally, add new question to the table
    response = table.put_item(
        Item={
            'id': 'lastTenQuestion',
            'questionsTableId': item['id'],
            'timestamp': item['timestamp'],
            'text': item['question'],
        }
    )
#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os
import json
import uuid
import time
import datetime
import random


#####
# INCREMENT TOTAL QUESTIONS HELPER FUNCTIONS
#####

# get sample questions from dynamodb
def postTotalQuestions(request):
    
    # get parameters
    params = json.loads(request.body)
    
    # connect to dynamodb
    awsRegion = os.environ.get('AWS_REGION')
    tableName = os.environ.get('USERS_TABLE')
    db = boto3.resource('dynamodb', region_name=awsRegion)
    table = db.Table(tableName)
    
    # first get user
    # then update total questions already seen
    # if there is an error, send it back
    try:
        
        user = table.get_item(
            Key={
                'username': params['username']
            }
        )
        
        newTotalQuestions = user['Item']['questionsRetrieved'] + params['n']
        newSuccessRatio = user['Item']['correctQuestions'] / newTotalQuestions
        
        response = table.update_item(
            Key={
                'username': params['username']
            },
            UpdateExpression='SET questionsRetrieved = :newTotalQuestions, successRatio = :newSuccessRatio',
            ExpressionAttributeValues={
                ':newTotalQuestions': newTotalQuestions,
                ':newSuccessRatio': newSuccessRatio,
            }
        )
        
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # reply indicating question were updated successefully
    return JsonResponse({
            'message': 'all okay',
            'status': 200
        })
#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os

# other imports
from api.helpers.helpers import getDynamoTable
from api.helpers.helpers import getDynamoClient

#####
# GET HOME PAGE CARD INFO
####
def getHomeText(request):
    
    # get query parameters
    card = request.GET.get('card')
    username = request.GET.get('username')
    
    # return information according to type of card
    if card == 'userStats':
        return getUserStats(username)
    elif card == 'generalStats':
        return getGeneralStats()
    elif card == 'newQuestions':
        return getNewQuestions()
    elif card == 'newForumQuestions':
        return getNewForumQuestions()


#####
# HELPER FUNCTIONS
#####

def getUserStats(username):
    
    # connect to dynamodb
    table = getDynamoTable('USERS_TABLE')
    
    # get current user entry
    # if an error occurs, send back that error
    try:
        response = table.get_item(
            Key={
                'username': username
            }
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return data to frontend
    return JsonResponse({
        'message': 'all okay',
        'status': 200,
        'questionsRetrieved': response['Item']['questionsRetrieved'],
        'correctQuestions': response['Item']['correctQuestions'],
        'successRatio': (round(response['Item']['successRatio'] * 100, 2)),
    })

def getGeneralStats():
    
    # connect to dynamodb
    client = getDynamoClient()
    
    # get general stats
    # if an error occurs, send back that error
    try:
        
        response = client.describe_table(TableName=os.environ.get('USERS_TABLE'))
        totalUsers = response['Table']['ItemCount']
        
        response = client.describe_table(TableName=os.environ.get('QUESTIONS_TABLE'))
        totalQuestions = response['Table']['ItemCount']
        
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return data to frontend
    return JsonResponse({
        'message': 'all okay',
        'status': 200,
        'totalUsers': totalUsers,
        'totalQuestions': totalQuestions,
    })
    
def getNewQuestions():
    
    # connect to dynamodb
    table = getDynamoTable('LAST_TEN_QUESTIONS_TABLE')
    
    # get last ten questions
    # only the text is returned
    # if an error occurs, send back that error
    try:
        response = table.query(
            ProjectionExpression='#myText',
            KeyConditionExpression='id = :partitionkeyval',
            ExpressionAttributeValues={
                ':partitionkeyval': 'lastTenQuestion',
            },
            ExpressionAttributeNames={
                '#myText': 'text'
            },
            ScanIndexForward=False,
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return data to frontend
    return JsonResponse({
        'message': 'all okay',
        'status': 200,
        'questions': response['Items'],
    })
    
def getNewForumQuestions():
    
    # return data to frontend
    return JsonResponse({
        'message': 'all okay',
        'status': 200,
    })
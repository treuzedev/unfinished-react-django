#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os
import random


#####
# GET SAMPLE QUESTIONS HELPER FUNCTIONS
#####

# get sample questions from dynamodb
def getQuestions():
    
    # connect to dynamodb
    awsRegion = os.environ.get('AWS_REGION')
    tableName = os.environ.get('QUESTIONS_TABLE')
    db = boto3.resource('dynamodb', region_name=awsRegion)
    table = db.Table(tableName)
    
    # retrieve 13 random questions
    # if an error occurs, send back that error
    randomNumber = random.randint(0, 3)
    try:
        if randomNumber > 1:
            response = table.scan(
                FilterExpression=Attr('randomInt').gte(500000)
            )
        elif randomNumber <= 1:
            response = table.scan(
                FilterExpression=Attr('randomInt').lte(500000)
            )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return questions
    return JsonResponse({
            'message': 'all okay',
            'questions': response['Items'],
            'status': 200
        })
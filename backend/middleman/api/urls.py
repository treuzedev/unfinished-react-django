#####
# IMPORTS
#####

# django imports
from django.urls import path

# views imports
from . import views

urlpatterns = [
    
    path('try', views.sampleQuestions, name='sampleQuestions'),
    path('new-question', views.newQuestion, name='newQuestion'),
    path('home', views.home, name='home'),
    path('play', views.questions, name='play'),
    path('user-stats/add-total-questions', views.totalQuestions, name='totalQuestions'),
    path('user-stats/add-correct-questions', views.correctQuestions, name='correctQuestions'),
    
]
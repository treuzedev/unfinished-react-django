#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# helpers
from api.helpers.getQuestions import getQuestions
from api.helpers.postTotalQuestions import postTotalQuestions
from api.helpers.postCorrectQuestions import postCorrectQuestions
from api.helpers.getSampleQuestions import getSampleQuestions
from api.helpers.postQuestion import postQuestion
from api.helpers.getHomeText import getHomeText

#####
# VIEWS
#####

# an api call to send back sample questions
def sampleQuestions(request):
    
    # only accept get requests
    if request.method == 'GET':
        return getSampleQuestions()
    else:
        return JsonResponse({
            'message': 'only get requests allowed',
            'status': 403
        })
        
# an api call to send back game questions
def questions(request):
    
    # only accept get requests
    if request.method == 'GET':
        return getQuestions()
    else:
        return JsonResponse({
            'message': 'only get requests allowed',
            'status': 403
        })

# an api call to post a new question to dynamodb
@csrf_exempt
def newQuestion(request):
    
    # only accept post requests
    if request.method == 'POST':
        return postQuestion(request)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403
        })
        
# an api call to get the home page card info
def home(request):
    
    # only accept get methods
    if request.method == 'GET':
        return getHomeText(request)
    else:
        return JsonResponse({
            'message': 'only get request allowed',
            'status': 403
        })
        
# an api call to increment total questions in user's stats
@csrf_exempt
def totalQuestions(request):
    
    # only accept post methods
    if request.method == 'POST':
        return postTotalQuestions(request)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403,
        })
        
# an api call to increment correct questions in user's stats
@csrf_exempt
def correctQuestions(request):
    
    # only accept post methods
    if request.method == 'POST':
        return postCorrectQuestions(request)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403,
        })
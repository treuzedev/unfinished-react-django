#####
# REMOTE STATE + CONFIG
#####

terraform {

  required_version = "~> 0.14.7"

  backend "s3" {
    bucket  = "eks-project-1"
    key     = "treuze/terraform/auth/terraform.tfstate"
    region  = "eu-west-1"
    encrypt = true
  }

#   required_providers {

#     # aws = {
#     #   source  = "hashicorp/aws"
#     #   version = "~> 3.29.0"
#     # }

#     # template = {
#     #   source  = "hashicorp/template"
#     #   version = "~> 2.1.2"
#     # }

#   }

}

#####
# PROVIDER
#####

provider "aws" {
  region = "eu-west-1"
}


#####
# RESOURCES
#####

##### create vpc
resource "aws_vpc" "vpc" {

  cidr_block           = var.vpc_details.cidr_block
  enable_dns_hostnames = true

  tags = {
    Env    = var.env
    Name   = var.vpc_details.name
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

}


##### create subnets
resource "aws_subnet" "vpc_subnets" {

  count = length(var.subnet_details.cidr_block)

  availability_zone       = var.subnet_details.az[count.index]
  cidr_block              = var.subnet_details.cidr_block[count.index]
  map_public_ip_on_launch = length(regexall(var.pub_subnet_name, var.subnet_details.name[count.index])) == 1 ? true : false
  vpc_id                  = aws_vpc.vpc.id

  tags = {
    Env    = var.env
    Name   = var.subnet_details.name[count.index]
    Owner = var.owner
    Project = var.project_name
    Tier   = length(regexall(var.pub_subnet_name, var.subnet_details.name[count.index])) == 1 ? "public" : "private"
    Region = var.region

}


##### create ip for ngw
resource "aws_eip" "ngw_ip" {

  vpc = true

  tags = {
    Env    = var.env
    Name   = var.ngw_eip_name
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

}


##### create ngw gateway id
resource "aws_nat_gateway" "vpc_ngw" {

  allocation_id = aws_eip.ngw_ip.id
  subnet_id     = aws_subnet.vpc_subnets[var.ngw_subnet].id

  tags = {
    Env    = var.env
    Name   = var.ngw_name
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

}


##### create internet gateway
resource "aws_internet_gateway" "vpc_igw" {

  vpc_id = aws_vpc.vpc.id

  tags = {
    Env    = var.env
    Name   = var.igw_name
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

}


##### create default private route table
resource "aws_route_table" "priv_route_table_default" {

  route {

    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.vpc_ngw.id

  }

  vpc_id = aws_vpc.vpc.id

  tags = {
    Env    = var.env
    Name   = var.priv_route_table_default
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

}


##### create default public route table
resource "aws_route_table" "pub_route_table_default" {

  route {

    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vpc_igw.id

  }

  tags = {
    Env    = var.env
    Name   = var.pub_route_table_default
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

  vpc_id = aws_vpc.vpc.id

}


##### associate subnets with corresponding route tables
resource "aws_route_table_association" "rt_ass" {

  count = length(aws_subnet.vpc_subnets)

  subnet_id      = aws_subnet.vpc_subnets[count.index].id
  route_table_id = length(regexall(var.pub_subnet_name, aws_subnet.vpc_subnets[count.index].tags.Name)) == 1 ? aws_route_table.pub_route_table.id : aws_route_table.priv_route_table.id

}


##### create rds sg
resource "aws_security_group" "rds" {

  description = "a sg for a rds instance; allow all traffic from this sg"
  vpc_id      = aws_vpc.vpc.id

  ingress {

    description = "all self"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self = true

  }
  
  egress {

    description = "allow all outgoing traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Env    = var.env
    Name   = var.sg_rds
    Owner = var.owner
    Project = var.project_name
    Region = var.region
  }

}

#####
variable "owner" {

  description = "person(s) resposible for launching network infrastructure; defaults to doublethink"
  default = "doublethink"

}


#####
variable "region" {

  description = "aws region; defaults to eu-west-1"
  default = "eu-west-1"

}


#####
variable "env" {

  description = "environment tag value; defaults to dev"
  default = "dev"

}


#####
variable "project_name" {

  description = "project name; defaults to treuze"
  default = "treuze"

}


#####
variable "vpc_details" {

  description = "a variable to define vpc details for treuze network"
  
  default = {
    name       = "treuze-main-vpc",
    region     = "eu-west-1",
    cidr_block = "10.10.10.10/24"
  }
  
}


#####
variable "subnet_details" {

  description = "vpc subnet configuration; it is a map where each key is an array; the keys's names are name, az, and cidr_block; they all come with default values for 4 subnets, two public and two private, both in the same azs; the name of a public subnet must match the value of the variable pub_subnet_name; for example, if name is treuze-subnet-pub-1, then pub_subnet_name must be treuze-subnet-pub"

  default = {
    name = [
      "treuze-subnet-pub-1",
      "treuze-subnet-priv-1"
    ],
    az = [
      "eu-west-1a",
      "eu-west-1a"
    ],
    cidr_block = [
      "10.10.10.0/28",
      "10.10.10.16/28"
    ]
  }

}


#####
variable "pub_subnet_name" {

  description = "vpc public subnet name; needs to match the name defined in subnet_details in order for terraform to correctly identify subnets as public or private."
  default = "treuze-subnet-pub"

}



#####
variable "ngw_eip_name" {

  description = "name for ngw ip allocation; defaults to treuze-ngw-ip"
  default = "treuze-ngw-ip"

}



#####
variable "ngw_subnet" {

  description = "where the nat gateway lives; its an index, corresponding to a certain cidr_block, specified in the sub_details.cidr array; defaults to 0, treuze-subnet-pub-1"
  default = 0

}



#####
variable "ngw_name" {

  description = "tag name for ngw; defaults to treuze-nat"
  default = "treuze-nat"

}


#####
variable "igw_name" {

  description = "tag name for the internet gateway; defaults to treuze-igw"
  default = "treuze-igw"

}


#####
variable "priv_route_table_name_default" {

  description = "name for the default private route table; defaults to treuze-rt-priv-default."
  default = "treuze-rt-priv-default"

}


#####
variable "pub_route_table_name_default" {

  description = "name for the default public route table; defaults to treuze-rt-pub-default."
  default = "treuze-rt-pub-default"

}


#####
variable "sg_rds" {

  description = "name for rds security group; defaults to treuze-sg-rds"
  default = "treuze-sg-rds"

}

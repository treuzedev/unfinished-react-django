#####
# REMOTE STATE + CONFIG
#####

terraform {

  required_version = "~> 0.14.7"

  backend "s3" {
    bucket  = "eks-project-1"
    key     = "treuze/terraform/auth/terraform.tfstate"
    region  = "eu-west-1"
    encrypt = true
  }

#   required_providers {

#     # aws = {
#     #   source  = "hashicorp/aws"
#     #   version = "~> 3.29.0"
#     # }

#     # template = {
#     #   source  = "hashicorp/template"
#     #   version = "~> 2.1.2"
#     # }

#   }

}

#####
# PROVIDER
#####

provider "aws" {
  region = "eu-west-1"
}


#####
# RESOURCES
#####
#####
# IMPORTS
#####

# django imports
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required

# helpers
from api.helpers.register import registerUser
from api.helpers.login import loginUser
from api.helpers.getUserInfo import getUserInfo
from api.helpers.changeProfileInfo import changeProfileInfo
from api.helpers.uploadPhotoToS3AndDynamo import uploadPhotoToS3AndDynamo

# jwt auth
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework_simplejwt.authentication import JWTAuthentication

# system imports 
import json

#####
# VIEWS
#####

# an api call to register a user
@csrf_exempt
def register(request):
    
    # only allow post methods
    if request.method == 'POST':
        return registerUser(request)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403
        })
        
# an api call to login a user
@csrf_exempt
def login(request):
    
    # only allow post methods
    if request.method == 'POST':
        return loginUser(request)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403
        })

# an api call to logout a user    
@csrf_exempt
@api_view(['POST'])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def checkToken(request):
    return JsonResponse({
            'message': 'token is valid',
            'status': 200
        })
        
# an api call to retrieve user info from dynamo
def userInfo(request):
    
    # only allow get requests
    if request.method == 'GET':
        user = request.GET.get('u')
        return getUserInfo(user)
    else:
        return JsonResponse({
            'message': 'only get requests allowed',
            'status': 403
        })
        
# change profile fields in dyanmo    
@csrf_exempt
def profileInfo(request, u):
    
    # only allow post methods
    if request.method == 'POST':
        params = json.loads(request.body)
        return changeProfileInfo(u, params)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403
        })
        
# upload photo to s3 and update dynamo
@csrf_exempt
def uploadPhoto(request):
    
    # only allow post methods
    if request.method == 'POST':
        return uploadPhotoToS3AndDynamo(request)
    else:
        return JsonResponse({
            'message': 'only post requests allowed',
            'status': 403
        })
    
#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import json
import requests
import time
import os
import base64

# other imports
from api.helpers.helpers import getDynamoTable


#####
# GET USER INFO
#####

# login user
def uploadPhotoToS3AndDynamo(request):
    
    # load data
    params = json.loads(request.body)
    
    # write image to tmp folder
    filename = f"{str(int(time.time()))}.{params['type']}"
    path = f"/tmp/{filename}"
    f = open(path, 'wb')
    f.write(base64.b64decode(params['data']))
    f.close()
    
    # connect to dynamodb
    usersTable = getDynamoTable('USERS_TABLE')
    
    # connect to s3
    TREUZE_BUCKET = os.environ.get('TREUZE_BUCKET')
    s3 = boto3.client('s3')
    
    # first put image in the bucket
    # then update user info in dynamo
    # finally, delete tmp image
    # if an error occurs, send back that error and delete tmp image
    try:
        
        profilePictureKey = f"profile-photos/{params['username']}/{filename}"
        
        response = s3.upload_file(path, TREUZE_BUCKET, profilePictureKey)
        
        response = usersTable.update_item(
            Key={
                'username': params['username'],
            },
            UpdateExpression='SET profilePictureKey = :profilePictureKey',
            ExpressionAttributeValues={
                ':profilePictureKey': profilePictureKey,
            },
        )
        
        deleteFile(path)
        
    except botocore.exceptions.ClientError as error:
        deleteFile(path)
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
        
    # return user token
    return JsonResponse({
        'message': 'photo uploaded successfully',
        'status': 200,
    })

# delete file from tmp folder   
def deleteFile(path):
    os.remove(path)
    
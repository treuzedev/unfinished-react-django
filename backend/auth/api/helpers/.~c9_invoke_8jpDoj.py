#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse
from django.db import IntegrityError

# # custom user model
from api.models import User

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os
import json
import uuid


#####
# REGISTER
#####

def registerUser(request):
    
    # get user parameters to register
    params = json.loads(request.body)
    
    # try to register a new user
    # if it fails, it means the email is already taken
    # send error response accordingly
    try:
        user = User.objects.create_user(params['username'], params['email'], params['password'])
        user.save()
    except IntegrityError as e:
        return JsonResponse({
            'message': f'{e.__cause__}',
            'status': 512
        })
        
    # create an entry in dynamodb for middleman service to use
    # connect to dynamodb
    awsRegion = os.environ.get('AWS_REGION')
    tableName = os.environ.get('USERS_TABLE')
    db = boto3.resource('dynamodb', region_name=awsRegion)
    table = db.Table(tableName)
    
    # generate unique values for each user
    
    # try to create an user entry
    # if there is an error, send it back
    try:
        response = table.put_item(
            Item={
                'id': userId,
                'username': params['username'],
                'email': params['email'],
                'questionsRetrieved': 0,
                'questionsAnswered': 0,
                'correctQuestions': 0,
            },
            ConditionExpression='attribute_not_exists(id)'
        )
    except botocore.exceptions.ClientError as error:
        print(error)
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return a response saying user is registered
    return JsonResponse({
        'message': 'user is registered',
        'status': 200
    })
#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore

# custom user model
from api.models import User

# other imports
from api.helpers.helpers import getDynamoTable


#####
# CHANGE PROFILE FIELDS
#####

# login user
def changeProfileInfo(user, params):
    
    # act according to field to change
    if params['field'] == 'email':
        response, status = changeEmail(user, params['oldValue'], params['value'])
    elif params['field'] == 'username':
        response, status = changeUsername(user, params['oldValue'], params['value'], params['email'])
    elif params['field'] == 'password':
        response, status = changePassword(params['value'], params['email'])
    else:
        return JsonResponse({
            'status': 500,
            'message': f"error! it is not possible to change {params['field']}",
        })
        
    # change values in local db if update was successful
    if status == 200:
        
        user = User.objects.get(username=user)
        
        if params['field'] == 'email':
            user.email = params['value']
        elif params['field'] == 'username':
            user.username = params['value']
        elif params['field'] == 'password':
            user.set_password(params['value'])
            
        user.save()
    
    # return json response
    return response
    
    
#####
# HELPERS FUNCTIONS
#####

#####
def changeEmail(user, oldValue, value):
    
    # get dynamo table
    emailsTable = getDynamoTable('EMAILS_TABLE')
    usernamesTable = getDynamoTable('USERNAMES_TABLE')
    
    # get password
    # delete old email
    # create new email entry
    # update email in usernames and user stats table
    # send error if something fails
    try:
        
        response = emailsTable.get_item(
            Key={
                'email': oldValue,
            },
        )
        
        password = response['Item']['password']
        
        response = emailsTable.delete_item(
            Key={
                'email': oldValue
            }
        )
        
        response = emailsTable.put_item(
            Item={
                'email': value,
                'password': password,
            },
        )
        
        response = usernamesTable.update_item(
            Key={
                'username': user,
            },
            UpdateExpression='SET email = :email',
            ExpressionAttributeValues={
                ':email': value,
            },
        )
        
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode'],
            }), 502
            
    # return message saying change went okay
    return JsonResponse({
        'message': f"email changed successfully",
        'status': 200,
        'changedField': 'email',
    }), 200
 
#####    
def changeUsername(user, oldValue, value, email):
    
    # get dynamo table
    usernamesTable = getDynamoTable('USERNAMES_TABLE')
    emailsTable = getDynamoTable('EMAILS_TABLE')
    
    # first delete current username entry
    # then create a new username entry
    # finally, update username in emails table
    try:
        
        response = usernamesTable.delete_item(
            Key={
                'username': oldValue,
            },
        )
        
        response = usernamesTable.put_item(
            Item={
                'username': value,
                'email': email,
            },
        )
        
        response = emailsTable.update_item(
            Key={
                'email': email,
            },
            UpdateExpression='SET username = :username',
            ExpressionAttributeValues={
                ':username': value,
            },
        )
        
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            }), 502
            
    # return message saying change went okay
    return JsonResponse({
        'message': f"username changed successfully",
        'status': 200,
        'changedField': 'username',
    }), 200
    
#####
def changePassword(value, email):
    
    # get dynamo table
    table = getDynamoTable('EMAILS_TABLE')
    
    # update new password value
    try:
        response = table.update_item(
            Key={
                'email': email
            },
            UpdateExpression='SET password = :password',
            ExpressionAttributeValues={
                ':password': value
            }
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            }), 502
            
    # return message saying change went okay
    return JsonResponse({
        'message': f"password changed successfully",
        'status': 200,
        'changedField': 'password',
    }), 200
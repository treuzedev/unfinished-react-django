// // // // //
// IMPORTS
// // // // //

// react-bootstrap components
import Button from 'react-bootstrap/Button';

// next imports
import { useRouter } from 'next/router';

// css
import styles from '../styles/AllowReload.module.css';

// // // // //
// CUSTOM COMPONENT
// // // // //
export default function AllowReload({ data }) {
    
    // allow reload to curront page
    const router = useRouter();
    function allowReload() {
        router.reload();
        return true;
    }
    
    // render component
    return (
        
        <div className={`text-center ${styles.main}`}>
            <p>{data.status}</p>
            <p>{data.message}</p>
            <Button className={`${styles.button} shadow-none actionButton ripple`} onClick={allowReload}>Reload</Button>
        </div>    
        
    );
    
}
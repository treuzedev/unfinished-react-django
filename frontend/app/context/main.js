// // // // //
// IMPORTS
// // // // //

// react context
import { createContext, useContext, useState, useEffect } from 'react';

// helpers
import { parseRefreshToken } from '../js/helpers.js';


// // // // //
// CONTEXT
// // // // //

// create and use a context
const AppContext = createContext();

export function useAppContext() {
    return useContext(AppContext);
}

// export context as a provider
export function AppWrapper({ children }) {
    
    // app wide context
    const [isUserLoggedIn, setUserLoggedInState] = useState(false);
    const [token, setToken] = useState('');
    const [username, setUsername] = useState('');
    const [trial, setTrial] = useState(false);
    const [questions, setQuestions] = useState(false);
    const [answers, setAnswers] = useState(false);
    const [score, setScore] = useState(false);
    
    const context = {
        isUserLoggedIn: isUserLoggedIn,
        token: token,
        username: username,
        trial: trial,
        questions: questions,
        answers: answers,
        score: score
    };
    
    const setContext = {
        isUserLoggedIn: setUserLoggedInState,
        token: setToken,
        username: setUsername,
        trial: setTrial,
        questions: setQuestions,
        answers: setAnswers,
        score: setScore
    };
    
    // // check auth everytime a page loads
    // useEffect(async () => {
    //     await getSessionStatus(context, setContext);
    // }, []);
    
    // use context as a provider
    // it is going to wrap main app
    return (
        <AppContext.Provider value={[context, setContext]}>
            {children}
        </AppContext.Provider>
    );
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //
export async function getSessionStatus(context, setContext) {
    
    // first check if there is a token in memory and if that token is valid
    // if there is no token or if token is not valid, check if there is a refresh token in local storage
    // if there is no refresh token, then user needs to login
    // if there is a refresh token, try to get a new access token
    // if the refresh token is not valid, user needs to login
    // set username if possible
    // make sure trial variable is false if user is logged in
    if (context.token !== '') {
        
        if (await checkToken(context, setContext)) {
            getUsername(context, setContext);
            setContext.trial(false);
            return true;
        } else {;
            
            if (await getToken(context, setContext)) {
                return true;
            } else {
                return false;
            }
        }
        
    } else {
        
        if (await getToken(context, setContext)) {
            return true;
        } else {
            return false;
        }
    }
    
}

// check if token is valid
async function checkToken(context, setContext) {
    
    // check token with django api
    // return is a token bool, if true, it means token is valid
    const response = await fetch('/api/token/check', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(context.token)
    });
    
    const data = await response.json();
    
    return data.token;
}

// get token
async function getToken(context, setContext) {
    
    // get token from local storage, if any
    let refreshToken = window.localStorage.getItem('treuzeRefreshToken');
    
    // if there is a refresh token, try to get a new access token
    // if there is no refresh token, user needs to log in
    // set username if possible
    // make sure trial is set to false
    if (refreshToken) {
        
        refreshToken = parseRefreshToken(refreshToken);
        
        setContext.username(refreshToken['username']);
        setContext.trial(false);
        
        const response = await fetch('/api/token/refresh', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'refresh': refreshToken['refresh']
            })
        });
        
        const data = await response.json();
        
        // if no tokens came back, user needs to login
        // else, return data to update context in the component
        if (data.token === false) {
            return false;
        } else {
            setContext.isUserLoggedIn(true);
            setContext.token(data.token);
            window.localStorage.setItem('treuzeRefreshToken', `${refreshToken.username}=${data.refresh}`);
            return true;
        }
        
    } else {
        return false;
    }
    
}

// update context username
// should be used only if user is logged in, ie, has a refresh token
export function getUsername(context, setContext) {
    let refreshToken = window.localStorage.getItem('treuzeRefreshToken');
    const username = parseRefreshToken(refreshToken)['username'];
    setContext.username(username);
}
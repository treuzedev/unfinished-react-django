// // // // //
// IMPORTS
// // // // //

// react bootstrap components
import Tab from 'react-bootstrap/Tab';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

// next components
import Head from 'next/head';
import { useRouter } from 'next/router';

// react
import { useEffect, useState } from 'react';

// custom components
import SubmitModal from '../../components/SubmitModal.js';
import TzNavbar from '../../components/Navbar.js';
import TzFooter from '../../components/Footer.js';
import LoadingSpinner from '../../components/LoadingSpinner.js';
import AllowReload from '../../components/AllowReload.js';

// css
import styles from '../../styles/[user].module.css';
import Emoji from 'a11y-react-emoji';

// app context
import { useAppContext, getSessionStatus } from '../../context/main.js';

const appContext = {
    context: null,
    setContext: null
};


// // // // //
// PAGE
// // // // //
export default function Profile() {
    
    // get context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // router for logged in redirection
    const router = useRouter();
    
    // show loading spinner until session is know
    const [loadingSpinner, setLoadingSpinner] = useState(true);
    
    // user info
    const [userInfo, setUserInfo] = useState({});
    
    // show a modal indicating the result of changing a profile field
    const [modalState, setModalState] = useState({});
  
    // redirect user to login page if not logged in
    // retrieve user info from dynamo
    // use effect so that router can be used
    useEffect(async () => {
    
        // redirection / show body
        // the first time router is rendered it is undefined
        if (router.query.user) {
            if (await !getSessionStatus(appContext.context, appContext.setContext)) {
                router.push('/login');
            } else {
                setUserInfo(await getUserInfo(router.query.user));
                setLoadingSpinner(false);
            }
        }
        
    }, [router.query.user]);

    // render page
    return (
        
        <div className="flexGrowPage">

            <Head>
                <title>TZ - Profile</title>
            </Head>
            
            <TzNavbar setLoadingSpinner={setLoadingSpinner} currentPage={'profile'} />
            
            {
            
                loadingSpinner
                
                ? <LoadingSpinner />
                
                : <ProfileBody 
                    userInfo={userInfo} 
                    setLoadingSpinner={setLoadingSpinner} 
                    modalState={modalState} 
                    setModalState={setModalState}
                    setUserInfo={setUserInfo}
                />
            }
            
            <TzFooter />
        
        </div>    
        
    );
    
}


// // // // //
// HELPER COMPONENTS
// // // // //
function ProfileBody({ userInfo, setLoadingSpinner, modalState, setModalState, setUserInfo }) {
    
    // allow redirects
    const router = useRouter();
    
    // allow reload if fetching profile details fail
    const [allowReload, setAllowReload] = useState(false);
    
    // variables to hold values inside inputs
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    
    // variables to values that will be updated
    const [oldUsername, setOldUsername] = useState('');
    const [oldEmail, setOldEmail] = useState('');
    const [oldPassword, setOldPassword] = useState('');
    
    // display profile picture, if any
    const [imageSrc, setImageSrc] = useState('');
    
    // allow reload if fetching profile details fail
    // first time component is rendered userInfo is undefined
    useEffect(() => {
        if (userInfo) {
            if (userInfo['status'] === 200) {
                setAllowReload(false);
                setUsername(userInfo['userInfo']['username']);
                setEmail(userInfo['userInfo']['email']);
                setPassword(userInfo['userInfo']['password']);
                setOldUsername(userInfo['userInfo']['username']);
                setOldEmail(userInfo['userInfo']['email']);
                setOldPassword(userInfo['userInfo']['password']);
                if (userInfo['userInfo']['photo']['status'] === 200) {
                    setImageSrc(userInfo['userInfo']['photo']['data']);
                }
            } else {
                setAllowReload(true);
            }
        }
    }, [userInfo]);
    
    // handle fields change
    function handleFieldChange (event) {
        if (event.target.id === 'email') {
            setEmail(event.target.value);
        } else if (event.target.id === 'username') {
            setUsername(event.target.value);
        } else if (event.target.id === 'password') {
            setPassword(event.target.value);
        }
    } 
    
    // handle individual form submit
    async function handleSubmit(event) {
        
        // prevent default submission
        event.preventDefault();
        
        // create data to sendo to backend
        let data = {};
        
        if (event.target.id === 'emailForm') {
            data = {
                'field': 'email',
                'value': email,
                'oldValue': oldEmail,
            };
        } else if (event.target.id === 'usernameForm') {
            data = {
                'field': 'username',
                'value': username,
                'oldValue': oldUsername,
                'email': email,
            };
        } else if (event.target.id === 'passwordForm') {
            data = {
                'field': 'password',
                'value': password,
                'oldValue': oldPassword,
                'email': email,
            };
        } else {
            // show modal indicating an error
        }
        
        // add username
        data['username'] = userInfo.userInfo.username;
        
        // wait for status to come back and show modal accordingly
        // the old value is now equal to the current value
        // reload page if username changed after modal close
        const response = await changeProfileInfo(data);
        
        if (response['changedField'] === 'email') {
            setOldEmail(email);
            setModalState(response);
        } else if (response['changedField'] === 'username') {
            setOldUsername(username);
            setModalState(response);
        } else if (response['changedField'] === 'password') {
            setOldPassword(password);
            setModalState(response);
        } else {
            setModalState(response);
        }
        
    }
    
    // handle photo submit
    async function handlePhotoSubmit(event) {
        
        // prevent normal form submition
        event.preventDefault();
        
        // get hidden input
        const input = document.getElementById('photo');
        
        // let user select file
        input.click();
        
        // when user chooses a photo, upload it
        // then, clear the files list
        input.onchange = async () => {
            
            // indicating loading
            setLoadingSpinner(true);
            
            // upload photo to backend
            const response = await uploadPhoto(input.files[0]);
            
            // show modal warning how the photo upload went
            setModalState(await response);
            
            // give control back to the user
            setLoadingSpinner(false);
            
            // // display new profile picture
            // if (response['status'] === 200) {
            //     // setImageSrc(URL.createObjectURL(input.files[0]));
            //     router.reload();
            // }
            
            // clear files from list
            input.value = null;
            
        };
        
    }
    
    // handle modal close
    function handleClose(event) {
        
        // hide modal and show loading spinner
        setModalState({
            modalShow: false,
        });
            
        // if changing a profile field other than photo, force user to relogin
        // if changing photo, reload photo into the browser
        if (event.target.value === 'email' || event.target.value === 'username' || event.target.value === 'password') {
            setLoadingSpinner(true);
            window.localStorage.removeItem('treuzeRefreshToken');
            appContext.setContext.isUserLoggedIn(false);
            appContext.setContext.token('');
            router.push('/login');
        } else if (event.target.value === 'photo') {
            router.reload();
        }
        
    }
    
    // render component
    return (
        
        <div className="flexGrowDiv d-flex justify-content-center flex-column lineHeight text-center">
        
            {
                allowReload ?
                
                <AllowReload data={userInfo} /> :
                
                <div className={`${styles.margin} ${styles.tabPane}`}>
                
                    <Tab.Container id="left-tabs" defaultActiveKey="first">
                    
                        <Row>
                        
                            <Col className="d-flex align-items-center" sm={3}>
                                <Nav variant="pills" className="flex-column">
                                    <Nav.Item>
                                        <Nav.Link eventKey="first">General Stuff</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="second">Tab 2</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </Col>
                            
                            <Col sm={9}>
                                <Tab.Content>
                                    <Tab.Pane eventKey="first">
                                        <CustomForm 
                                            formId="emailForm"
                                            handleSubmit={handleSubmit}
                                            label="Email"
                                            emoji=<Emoji symbol="📧" label="email"/>
                                            formType="email"
                                            placeholder="Enter email"
                                            onChange={handleFieldChange}
                                            value={email}
                                            controlId="email"
                                            text="Some email explanation"
                                            buttonValue="email"
                                            buttonText="Change Email"
                                        />
                                        <CustomForm 
                                            formId="usernameForm"
                                            handleSubmit={handleSubmit}
                                            label="Username"
                                            emoji=<Emoji symbol="👤" label="username"/>
                                            formType="text"
                                            placeholder="Enter username"
                                            onChange={handleFieldChange}
                                            value={username}
                                            controlId="username"
                                            text="Some username explanation"
                                            buttonValue="username"
                                            buttonText="Change Username"
                                        />
                                        <CustomForm 
                                            formId="passwordForm"
                                            handleSubmit={handleSubmit}
                                            label="Password"
                                            emoji=<Emoji symbol="🔑" label="password"/>
                                            formType="password"
                                            placeholder="Enter password"
                                            onChange={handleFieldChange}
                                            value={password}
                                            controlId="password"
                                            text="Some password explanation"
                                            buttonValue="password"
                                            buttonText="Change Password"
                                        />
                                        <CustomForm 
                                            formId="photoForm"
                                            handleSubmit={handlePhotoSubmit}
                                            label="Profile Photo"
                                            emoji=<Emoji symbol="🤳" label="profile photo"/>
                                            image={imageSrc}
                                            formType="file"
                                            controlId="photo"
                                            text="Some photo explanation"
                                            buttonValue="photo"
                                            buttonText="Change Photo"
                                        />
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="second">
                                        stuff 2
                                    </Tab.Pane>
                                </Tab.Content>
                            </Col>
                            
                        </Row>
                        
                    </Tab.Container>
                    
                    <SubmitModal modalState={modalState} handleClose={handleClose} />
                    
                </div>
                
            }
            
        </div>
        
    );
    
}

// individual forms for, for example, changing username, email, password..
function CustomForm({ formId, controlId, label, formType, placeholder, onChange, value, text, buttonValue, buttonText, handleSubmit, emoji, image }) {
    
    // render component
    return (
    
        <Form id={formId} noValidate validated={true} onSubmit={handleSubmit}>
            <Row className={`d-flex align-items-center ${styles.margin}`}>
                <Col sm={9}>
                    <Form.Group>
                        <Row className="justify-content-center">
                            <Form.Label>{label} {emoji}</Form.Label>
                        </Row>
                        <Row className="justify-content-center">
                            {
                                formId === 'photoForm'
                                ?
                                <input id={controlId} type={formType} className={styles.photoInput} encType="multipart-form-data" />
                                :
                                <Form.Control type={formType} placeholder={placeholder} onChange={onChange} value={value ? value : ''} id={controlId} />
                            }
                            {image && <img height="200" width="200" src={image} />}
                        </Row>
                        <Form.Text className="text-muted">{text}</Form.Text>
                    </Form.Group>
                </Col>
                <Col className={styles.buttonCol} sm={3}>
                    <Button className={`${styles.button} shadow-none actionButton ripple`} type="submit" value={buttonValue}>{buttonText}</Button>
                </Col>
            </Row>
        </Form>
        
    );
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //

// get user info from dynamo
// parse profile photo
async function getUserInfo(user) {
    
    // get data
    const response = await fetch(`/api/u/${user}`);
    const data = await response.json();
    
    // if a photo came back, parse it
    if (data['userInfo']['photo']['status'] === 200) {
        
        // decode b64 string into a bytes string
        const byteString = atob(data['userInfo']['photo']['data']);
        
        // create an array of bytes
        const byteArray = new Uint8Array(byteString.length);
        
        for (let i = 0; i < byteString.length; i++) {
            byteArray[i] = byteString.charCodeAt(i);
        }
        
        // use an array of bytes to create a file in memory
        const file = new File([byteArray], `profile-picture${data['userInfo']['photo']['fileExtension']}`);
    
        data['userInfo']['photo']['data'] = URL.createObjectURL(file);
        
    }
    
    // return user info
    return data;
    
}

// change profile field in dynamo
async function changeProfileInfo(body) {
    
    // first try to update values in dynamo
    const response = await fetch('/api/change-profile-info', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
    });
    
    const data = await response.json();
    
    // if dynamo was updated successfully, then update the values in the frontend
    // if not, only warn the user
    if (data.status === 200) {
        
        if (data['changedField'] === 'email') {
            return {
                formType: 'Profile Change',
                message: 'Email updated successfully!',
                buttonText: 'Nice',
                buttonValue: 'email',
                changedField: data['changedField'],
                modalShow: true,
            };
        } else if (data['changedField'] === 'username') {
            return {
                formType: 'Profile Change',
                message: 'Username updated successfully!',
                buttonText: 'Nice',
                buttonValue: 'username',
                changedField: data['changedField'],
                modalShow: true,
            };
        } else if (data['changedField'] === 'password') {
            return {
                formType: 'Profile Change',
                message: 'Password updated successfully!',
                buttonText: 'Nice',
                buttonValue: 'password',
                changedField: data['changedField'],
                modalShow: true,
            };
        } else {
            return {
                formType: 'Profile Change',
                message: `Oh no! Something went wrong. Please try again. (Error message: ${data['message']})`,
                buttonText: 'Try again',
                buttonValue: 'error',
                changedField: 'none',
                modalShow: true,
            };
        }
        
    } else {
        return {
            formType: 'Profile Change',
            message: `Oh no! Something went wrong. Please try again. (Error message: ${data['message']})`,
            buttonText: 'Try again',
            buttonValue: 'error',
            changedField: 'none',
            modalShow: true,
        };
    }
    
}

// upload photo to s3 and update dynamo
async function uploadPhoto(photo) {
    
    // first make sure the file is a photo and is of small size
    const validity = checkPhoto(photo);
    
    if (validity['status'] === 200) {
        
        // get photo type
        // since it passed checkPhoto, does not need further checking
        let type = '';
        if (photo.type === 'image/jpeg') {
            type = 'jpeg';
        } else if (photo.type === 'image/png') {
            type = 'png';
        } else if (photo.type === 'image/gif') {
            type = 'gif';
        }
        
        // create form data to send image to django
        // enconding type, multipart/form-data, is set in the input
        // the browser needs to set its own headers and boudaries for this type of request
        let formData = new FormData();
        formData.append('file', photo);
        formData.append('type', type);
        formData.append('username', appContext.context.username);
        
        const response = await fetch('/api/u/upload-photo', {
            method: 'POST',
            body: formData,
        });
        
        const data = await response.json();
        
        if (data['status'] === 200) {
            return {
                status: data['status'],
                formType: 'Photo Change',
                message: 'Profile photo changed successfully!',
                buttonText: 'AWSome',
                buttonValue: 'photo',
                changedField: 'photo',
                modalShow: true,
            };
        } else {
            return {
                status: data['status'],
                formType: 'Profile Change',
                message: `Oh no! Something went wrong. Please try again. (Error message: ${data['message']})`,
                buttonText: 'Try again',
                buttonValue: 'error',
                changedField: 'none',
                modalShow: true,
            };
        }
        
    } else {
        return {
            status: validity['status'],
            formType: 'Profile Change',
            message: `Oh no! Something went wrong. Please try again. (Error message: ${validity['message']})`,
            buttonText: 'Try again',
            buttonValue: 'error',
            changedField: 'none',
            modalShow: true,
        };
    }
    
}

// check photo details
function checkPhoto(photo) {
    
    // file extension and size (1MB)
    if (photo.type ==! 'image/jpeg' || photo.type ==! 'image/png' || photo.type ==! 'image/gif') {
        return {
            status: 500,
            message: 'Only .jpeg, .png or .gif file extensions allowed.'
        };
    } else {
        if (photo.size > 1048576) {
            return {
                status: 500,
                message: 'File size limit is 1MB.'
            }
        } else {
            return {
                status: 200,
            }
        }
    }
    
}
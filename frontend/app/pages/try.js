// // // // //
// IMPORTS
// // // // //

// next components
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router'

// react
import { useEffect, useState } from 'react';

// react bootstrap components
import Button from 'react-bootstrap/Button';

// custom components
import TzNavbar from '../components/Navbar.js';
import TzFooter from '../components/Footer.js';
import LoadingSpinner from '../components/LoadingSpinner.js';
import DisplayGameQuestions from '../components/DisplayGameQuestions.js';

// css
import styles from '../styles/try.module.css';

// helper functions

// app context
import { useAppContext, getSessionStatus } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
};

// // // // //
// PAGE
// // // // //
export default function Try() {
  
  // get context
  [appContext.context, appContext.setContext] = useAppContext();
  
  // router for logged in redirection
  const router = useRouter();
  
  // show loading spinner until session is know
  const [loadingSpinner, setLoadingSpinner] = useState(true);
  
  // display questions after they are loaded
  const [questions, setQuestions] = useState(<div></div>);
  
  // redirect user to home page if user is logged in
  // use effect so that router can be used
  // if user reached this page and is not logged in, the is trying the app for the first time - update context
  useEffect(async () => {
    
    if (await getSessionStatus(appContext.context, appContext.setContext)) {
        router.push('home');
    } else {
        setQuestions(await loadSampleQuestions());
        appContext.setContext.trial(true);
        setLoadingSpinner(false);
    }
    
  }, []);
  
  return (
    
    <div className="flexGrowPage">
    
      <Head>
        <title>TZ - Homepage</title>
      </Head>
      
      <TzNavbar setLoadingSpinner={setLoadingSpinner} currentPage={'try'} />
    
      {
        
        loadingSpinner
        
        ? <LoadingSpinner />
        
        : <TryBody questions={questions} />
        
      }
      
      <TzFooter />
    
    </div>  
  
  );
}


// // // // //
// CUSTOM COMPONENT
// // // // //

// a component to display everything except the navbar and footer
function TryBody({ questions }) {
  
  // show loading spinner until questions arrive
  const [loadingSpinner, setLoadingSpinner] = useState(true);
  
  // show a component to reload the page if questions status was not 200
  const [validQuestions, setValidQuestions] = useState(false);
  
  // check if questions that arrived are valid
  // if they are not, allow user to reload the page
  useEffect(() => {
    
    // loading spinner is not needed in either case
    setLoadingSpinner(false);
    
    if (questions.status === 200) {
      setValidQuestions(true);
    } else {
      return false;
    }
    
    // always return something
    return true;
    
  }, [])
  
  return (
    
    <div className="flexGrowDiv d-flex justify-content-center align-items-center flex-column lineHeight">
      {
        loadingSpinner
        ? <LoadingSpinner />
        : 
        validQuestions
        ? <DisplayGameQuestions questions={questions.questions} reviewMode={false} setLoadingSpinner={setLoadingSpinner} />
        : <AllowReload />
      }
    </div>
    
  );
  
}


// // // // //
// HELPER COMPONENTS
// // // // //

// a component to allow a page reload
function AllowReload() {
  
  // react hooks can only be called from inside react functions
  const router = useRouter();
  
  // reload page
  function reload() {
    
    // reload this page
    router.reload();
    
    // always return something
    return true;
    
  }
  
  // render component
  return(
    
    <div className="text-center">
      <p>There was a problem loading the questions. Click the button to retry loading the questions.</p>
      <Button className="actionButton shadow-none ripple" onClick={reload}>Reload</Button>
    </div>  
    
  );
  
}


// // // // //
// HELPERS
// // // // //

// a function to load sample questions from dynamodb via django backend api call
async function loadSampleQuestions() {
    const response = await fetch('/api/try');
    return await response.json();
}
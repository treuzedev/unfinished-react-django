// // // // //
// IMPORTS
// // // // //

// next components
import Head from 'next/head';
import { useRouter } from 'next/router';

// react
import { useEffect, useState } from 'react';

// custom components
import TzNavbar from '../components/Navbar.js';
import TzFooter from '../components/Footer.js';
import LoadingSpinner from '../components/LoadingSpinner.js';

// css
import styles from '../styles/index.module.css';

// font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestion, faLevelUpAlt, faMedal } from '@fortawesome/free-solid-svg-icons';

// app context
import { useAppContext, getSessionStatus } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
};

// // // // //
// PAGE
// // // // //
export default function Index() {
  
  // get context
  [appContext.context, appContext.setContext] = useAppContext();
  
  // router for logged in redirection
  const router = useRouter();
  
  // show loading spinner until session is know
  const [loadingSpinner, setLoadingSpinner] = useState(true);
  
  // redirect user to home page if user is logged in
  // use effect so that router can be used
  useEffect(async () => {
    
    if (await getSessionStatus(appContext.context, appContext.setContext)) {
      router.push('home');
    } else {
      setLoadingSpinner(false);
    }
    
  }, []);
  
  return (
    
    <div className="flexGrowPage">
    
      <Head>
        <title>TZ - Homepage</title>
      </Head>
      
      <TzNavbar setLoadingSpinner={setLoadingSpinner} currentPage={'tz'} />
    
      {
        
        loadingSpinner
        
        ? <LoadingSpinner />
        
        : <IndexBody />
        
      }
      
      <TzFooter />
    
    </div>  
  
  );
}


// // // // //
// CUSTOM COMPONENT
// // // // //
function IndexBody() {
  
  return (
    
    <div className="flexGrowDiv">
    
      <div className={`container-fluid ${styles.firstSection} d-flex align-items-center justify-content-center text-center`}>
        <div className="row no-gutters">
          <div className="col">
            <h1 className="logo">T R E U Z E</h1>
            <h1 className="titles">The place where knowledge earns you NFTs</h1>
          </div>
        </div>
      </div>
      
      <div className={`container-fluid ${styles.secondSection} d-flex align-items-center justify-content-center text-center`}>
        <div className="row no-gutters w-100">
          <div className="col-sm d-flex align-items-center justify-content-center">
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faQuestion} />
              <h1 className="titles whiteText">{`Solve\nQuizzes`}</h1>
            </div>
          </div>
          <div className="col-sm d-flex align-items-center justify-content-center">
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faLevelUpAlt} />
              <h1 className="titles whiteText">{`Gain\nXp`}</h1>
            </div>
          </div>
          <div className="col-sm d-flex align-items-center justify-content-center">
            <div className={styles.icon}>
              <FontAwesomeIcon icon={faMedal} />
              <h1 className="titles whiteText">{`Claim\nNFTs`}</h1>
            </div>
          </div>
        </div>
      </div>
      
      <div className={`container-fluid ${styles.thirdSection} justify-content-center text-center`}>
        <div className="row no-gutters">
          <div className="col-sm-6">
            <p className={styles.explainText}>
              Morbi efficitur metus nisi, et finibus quam rutrum eget. In nec ex lacus. Nam euismod vel odio nec pulvinar. Nunc placerat lorem a lectus sollicitudin feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sed erat nunc. Nullam vel mi molestie, dapibus turpis imperdiet, fermentum erat. Praesent pretium sapien orci, ut tincidunt magna porta eu.
            </p>
          </div>
          <div className="col-sm-6">
            <div className={styles.explainImage}>
            </div>
          </div>
        </div>
        <div className="row no-gutters">
          <div className="col-sm-6">
            <div className={styles.explainImage}>
            </div>
          </div>
          <div className="col-sm-6">
            <p className={styles.explainText}>
              Morbi efficitur metus nisi, et finibus quam rutrum eget. In nec ex lacus. Nam euismod vel odio nec pulvinar. Nunc placerat lorem a lectus sollicitudin feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sed erat nunc. Nullam vel mi molestie, dapibus turpis imperdiet, fermentum erat. Praesent pretium sapien orci, ut tincidunt magna porta eu.
            </p>
          </div>
        </div>
        <div className="row no-gutters">
          <div className="col-sm-6">
            <p className={styles.explainText}>
              Morbi efficitur metus nisi, et finibus quam rutrum eget. In nec ex lacus. Nam euismod vel odio nec pulvinar. Nunc placerat lorem a lectus sollicitudin feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sed erat nunc. Nullam vel mi molestie, dapibus turpis imperdiet, fermentum erat. Praesent pretium sapien orci, ut tincidunt magna porta eu.
            </p>
          </div>
          <div className="col-sm-6">
            <div className={styles.explainImage}>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    
  );
  
}
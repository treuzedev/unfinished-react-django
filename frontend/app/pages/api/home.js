// // // // //
// IMPORTS
// // // // //


// // // // //
// API CALL
// // // // //

// get cards info
export default async function handler(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_MIDDLEMAN_IP;
    const djangoPort = process.env.DJANGO_MIDDLEMAN_PORT;
    
    // request all coins, wait for response, parse as json
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/home?card=${request.query.card}&username=${request.query.username}`);
    
    const serverData = await serverResponse.json();
    
    // return backend response
    // status is always 200, as the backend always send a response
    // however, the login may have not been successful, so the true response is inside the next api response
    // this is done so there are no errors in the browser console
    return response.status(200).json(serverData);
    
}
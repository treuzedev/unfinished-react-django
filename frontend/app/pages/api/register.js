// // // // //
// IMPORTS
// // // // //


// // // // //
// API CALL
// // // // //

// retrieve all coins list from django server
export default async function handler(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_AUTH_IP;
    const djangoPort = process.env.DJANGO_AUTH_PORT;
    
    // request all coins, wait for response, parse as json
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request.body)
    });
    
    const serverData = await serverResponse.json();
    
    // return backend response
    return response.status(200).json({
        status: serverData.status,
        message: serverData.message
    });
    
}
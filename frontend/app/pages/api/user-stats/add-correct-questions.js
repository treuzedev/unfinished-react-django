// // // // //
// IMPORTS
// // // // //


// // // // //
// API CALL
// // // // //

// increment correct questions in dynamodb
export default async function handler(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_MIDDLEMAN_IP;
    const djangoPort = process.env.DJANGO_MIDDLEMAN_PORT;
    
    // try
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/user-stats/add-correct-questions`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request.body)
    });
    
    const serverData = await serverResponse.json();
    
    // return backend response
    // status is always 200, as the backend always send a response
    // however, posting a new question may have not been successful, so the true response is inside the next api response
    // this is done so that no errors are displayed in the browser console
    return response.status(200).json(serverData);
    
}
// // // // //
// IMPORTS
// // // // //

// css
// bootstrap needs to come first or it will override custom css
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';

// context
import { AppWrapper } from '../context/main.js';

function MyApp({ Component, pageProps }) {
  return (
    <AppWrapper>
      <Component {...pageProps} />
    </AppWrapper>
  );
}

export default MyApp;

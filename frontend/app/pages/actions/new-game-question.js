// // // // //
// IMPORTS
// // // // //

// next components
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router'

// react
import { useEffect, useState } from 'react';

// custom components
import TzNavbar from '../../components/Navbar.js';
import TzFooter from '../../components/Footer.js';
import LoadingSpinner from '../../components/LoadingSpinner.js';
import NewGameQuestion from '../../components/NewGameQuestion.js'

// css
import styles from '../../styles/new-game-question.module.css';

// app context
import { useAppContext, getSessionStatus, getUsername } from '../../context/main.js';

const appContext = {
    context: null,
    setContext: null
};

// // // // //
// PAGE
// // // // //
export default function NewGameQuestionPage() {
  
  // get context
  [appContext.context, appContext.setContext] = useAppContext();
  
  // router for logged in redirection
  const router = useRouter();
  
  // show loading spinner until session is know
  const [loadingSpinner, setLoadingSpinner] = useState(true);
  
  // first check if user is logged in and redirect if not
    useEffect(async () => {
        
        if (await getSessionStatus(appContext.context, appContext.setContext)) {
            setLoadingSpinner(false);
        } else {
            router.push('/login');
        }
        
    }, []);
  
  // render component
  return (
    
    <div className="flexGrowPage">
    
      <Head>
        <title>TZ - New Question</title>
      </Head>
      
      <TzNavbar />
    
      {
        
        loadingSpinner
        
        ? <LoadingSpinner />
        
        : <div className="flexGrowDiv justify-content-center">
            <NewGameQuestionBody />
          </div>
        
      }
      
      <TzFooter />
    
    </div>  
  
  );
}


// // // // //
// CUSTOM COMPONENT
// // // // //
function NewGameQuestionBody() {
  
  return (
    
    <div className={styles.margin}>
        
        <NewGameQuestion username={appContext.context.username} />
      
    </div>
    
  );
  
}
// // // // //
// IMPORTS
// // // // //

// next components
import Head from 'next/head';
import { useRouter } from 'next/router';

// react
import { useEffect, useState } from 'react';

// custom components
import TzNavbar from '../components/Navbar.js';
import TzFooter from '../components/Footer.js';
import UserForm from '../components/UserForm.js';
import LoadingSpinner from '../components/LoadingSpinner.js';

// app context
import { useAppContext, getSessionStatus } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
};


// // // // //
// PAGE
// // // // //
export default function Register() {
    
    // get context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // router for logged in redirection
    const router = useRouter();
    
    // show loading spinner until session is know
    const [loadingSpinner, setLoadingSpinner] = useState(true);
  
    // redirect user to home page if user is logged in
    // use effect so that router can be used
    useEffect(async () => {
    
        if (await getSessionStatus(appContext.context, appContext.setContext)) {
            router.push('home');
        } else {
            setLoadingSpinner(false);
        }
        
    }, []);
    
    // render page
    return (
        
        <div className="flexGrowPage">

            <Head>
                <title>TZ - Register</title>
            </Head>
            
            <TzNavbar setLoadingSpinner={setLoadingSpinner} currentPage={'register'} />
            
            {
            
                loadingSpinner
                
                ? <LoadingSpinner />
                
                : <div className="d-flex flexGrowDiv lineHeight justify-content-center">
                    <UserForm formType={'register'}/>
                  </div>
                
            }
            
            <TzFooter />
        
        </div>    
        
    );
    
}